import os
import socket
from datetime import timedelta

from flask import Flask
from redis import Redis, RedisError
from celery import Celery
from celery.bin import worker



# Connect to Redis
redis = Redis(host="redis", port=6379, db=0, socket_connect_timeout=2, socket_timeout=2)



def make_celery(app):
    celery = Celery(app.import_name,
                    backend=app.config['CELERY_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery

app = Flask(__name__)
app.config['CELERY_BACKEND'] = "redis://redis:6379/0"
app.config['CELERY_BROKER_URL'] = "redis://redis:6379/0"

app.config['CELERYBEAT_SCHEDULE'] = {
    'say-every-5-seconds': {
        'task': 'return_something',
        'schedule': timedelta(seconds=5)
    },
}
app.config['CELERY_TIMEZONE'] = 'UTC'

celery = make_celery(app)

@app.route("/")
def hello():
    try:
        visits = redis.incr("counter")
    except RedisError:
        visits = "<i>cannot connect to Redis, counter disabled</i>"

    html = "<h3>Hello {name}!</h3>" \
           "<b>Hostname:</b> {hostname}<br/>" \
           "<b>Visits:</b> {visits}<br/>" \
           "<b>Region: {region}</b>"

    return html.format(name=os.getenv("NAME", "world"), hostname=socket.gethostname(), visits=visits,
                       region=os.getenv("REGION", "None"))


@app.route("/add/<a>")
def add(a):
    a = float(a)
    r = celery_simple.apply_async(args=(a,))
    # r = celery_simple.delay(a, b)
    r.wait()
    # res = r.wait()
    res = r.get()
    # res = a + b
    return "Res: %s" % (res,)

@celery.task(name='return sum (10)')
def celery_simple(a):
    return a ** 2

def run_celery():
    w = worker.worker(app=celery)
    w.run(**{
        'loglevel': 'INFO',
        'traceback': True,
        'logfile': 'celery.log',
        # 'maxmemperchild': 25000,
        # 'max-tasks-per-child': 5,
        # 'concurrency': 2
    })



if __name__ == "__main__":
    from multiprocessing import Process

    p = Process(target=run_celery)
    p.start()
    app.run(host='0.0.0.0', port=80)