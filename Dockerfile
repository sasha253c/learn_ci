FROM python:3.6

WORKDIR /app
ADD . /app


#RUN apt-get update && \
#    apt-get upgrade -y && \
#    apt-get install -y  software-properties-common && \
#    add-apt-repository ppa:webupd8team/java -y && \
#    apt-get update && \
#    echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \
#    apt-get install -y oracle-java8-installer && \
#    apt-get clean
#
#RUN pip install -r requirements.txt
#RUN java -mx500m -cp model/stanford-ner-with-classifier.jar edu.stanford.nlp.ie.NERServer -port 9001 -loadClassifier english.all.3class.distsim.crf.ser.gz -outputFormat inlineXML

#RUN celery -A app.celery worker -l info &
EXPOSE 80

ENV NAME World
ENV C_FORCE_ROOT=1
# Run app.py when the container launches
CMD ["python", "app.py"]